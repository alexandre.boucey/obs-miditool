package page

import (
	"time"

	"midilink/internal/device"
	"midilink/internal/events"
	"midilink/internal/obs"
)

var knownBindingType = map[BindType]func(base BindingBase) Binding{}

type BindType string

const (
	StateNameOn         = "on"
	StateNameOff        = "off"
	StateNameUnassigned = "unassigned"

	BlinkDuration = 200 * time.Millisecond
)

type Binding interface {
	Activate()
	Deactivate()

	GetDeviceID() string
	GetInputID() string

	HandleOBS(event events.ObsEvent)
	HandleMIDI(event events.MidiEvent)
}

type BindingBase struct {
	deviceID string
	inputID  string

	obsClient     *obs.ClientWrapper
	deviceManager *device.Manager
	pageManager   *Manager
}

func NewBinding(bindType BindType, deviceID string, inputID string, obsClient *obs.ClientWrapper, deviceManager *device.Manager, pageManager *Manager) Binding {
	base := BindingBase{
		deviceID:      deviceID,
		inputID:       inputID,
		obsClient:     obsClient,
		deviceManager: deviceManager,
		pageManager:   pageManager,
	}

	if initFunc, ok := knownBindingType[bindType]; ok {
		return initFunc(base)
	}

	return nil
}

func (b *BindingBase) Deactivate() {
	b.setButtonState(StateNameUnassigned)
}

func (b *BindingBase) GetDeviceID() string {
	return b.deviceID
}

func (b *BindingBase) GetInputID() string {
	return b.inputID
}

func (b *BindingBase) setButtonState(stateName string) {
	d := b.deviceManager.GetDevice(b.deviceID)
	if d == nil {
		return
	}

	err := d.SetButtonState(b.inputID, stateName)
	if err != nil {
		return
	}
}

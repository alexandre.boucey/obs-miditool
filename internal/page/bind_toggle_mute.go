package page

import (
	"log/slog"

	obsevents "github.com/andreykaipov/goobs/api/events"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeToggleMute BindType = "toggle_mute"

type ToggleMuteBinding struct {
	BindingBase
	SourceName string `yaml:"sourceName"`
}

func init() {
	knownBindingType[TypeToggleMute] = func(base BindingBase) Binding {
		return &ToggleMuteBinding{BindingBase: base}
	}
}

func (b *ToggleMuteBinding) Activate() {
	muted, err := b.obsClient.GetInputMute(b.SourceName)
	if err != nil {
		slog.Warn("GetInputMute failed", slog.String("source_name", b.SourceName), logger.Error(err))
		return
	}

	stateName := StateNameOff
	if muted {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleMuteBinding) HandleOBS(event events.ObsEvent) {
	e, ok := event.Event.(*obsevents.InputMuteStateChanged)
	if !ok {
		return
	}

	if e.InputName != b.SourceName {
		return
	}

	stateName := StateNameOff
	if e.InputMuted {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleMuteBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	_, err := b.obsClient.ToggleMuteState(b.SourceName)
	if err != nil {
		slog.Warn("ToggleMuteState failed", slog.String("source_name", b.SourceName), logger.Error(err))
	}
}

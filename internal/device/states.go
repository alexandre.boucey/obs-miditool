package device

import "gitlab.com/gomidi/midi/v2"

type setStateAction interface {
	Message() midi.Message
}

type actionNoteOn struct {
	key      uint8
	velocity uint8
}

func (a actionNoteOn) Message() midi.Message {
	return midi.NoteOn(0, a.key, a.velocity)
}

type actionNoteOff struct {
	key uint8
}

func (a actionNoteOff) Message() midi.Message {
	return midi.NoteOffVelocity(0, a.key, 0)
}

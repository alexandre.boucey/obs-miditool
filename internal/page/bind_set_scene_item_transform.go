package page

import (
	"log/slog"
	"math"

	obsevents "github.com/andreykaipov/goobs/api/events"
	"github.com/andreykaipov/goobs/api/typedefs"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeSetTransform BindType = "set_transform"

type SetTransformBinding struct {
	BindingBase
	SourceName string `yaml:"sourceName"`
	SceneName  string `yaml:"sceneName"`
	PositionX  float64
	PositionY  float64
	Width      float64
	Height     float64
}

func init() {
	knownBindingType[TypeSetTransform] = func(base BindingBase) Binding {
		return &SetTransformBinding{BindingBase: base}
	}
}

func withTolerance(a, b float64) bool {
	diff := math.Abs(a - b)
	return diff < 0.001
}

func (b *SetTransformBinding) isSame(transform *typedefs.SceneItemTransform) bool {
	return withTolerance(transform.PositionX, b.PositionX) &&
		withTolerance(transform.PositionY, b.PositionY) &&
		withTolerance(transform.Width, b.Width) &&
		withTolerance(transform.Height, b.Height)
}

func (b *SetTransformBinding) Activate() {
	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	transform, err := b.obsClient.GetSceneItemTransform(b.SceneName, sceneItemId)
	if err != nil {
		slog.Warn("GetSceneItemTransform failed",
			slog.String("scene_name", b.SceneName),
			slog.Int("scene_item_id", sceneItemId),
			logger.Error(err),
		)
		return
	}

	stateName := StateNameOff
	if b.isSame(transform) {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *SetTransformBinding) HandleOBS(event events.ObsEvent) {
	e, ok := event.Event.(*obsevents.SceneItemTransformChanged)
	if !ok {
		return
	}

	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	if e.SceneName != b.SceneName || e.SceneItemId != sceneItemId {
		return
	}

	stateName := StateNameOff
	if b.isSame(e.SceneItemTransform) {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *SetTransformBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	_, err = b.obsClient.SetSceneItemTransform(b.SceneName, sceneItemId, b.PositionX, b.PositionY, b.Width, b.Height)
	if err != nil {
		slog.Warn("SetSceneItemTransform failed",
			slog.String("scene_name", b.SceneName),
			slog.Int("scene_item_id", sceneItemId),
			logger.Error(err),
		)
	}
}

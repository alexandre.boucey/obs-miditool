package updater

const (
	undefinedString = "v0.0.0"
)

var (
	version       = undefinedString
	commit        = undefinedString
	gitlabProject = ""
)

//goland:noinspection GoBoolExpressions
func IsReleaseVersion() bool {
	return version != undefinedString && version != ""
}

func VersionString() string {
	if !IsReleaseVersion() {
		return "DEV-" + commit
	}
	return version
}

func ReleaseVersionStringOrEmpty() string {
	if !IsReleaseVersion() {
		return ""
	}
	return VersionString()
}

package page

import (
	"log/slog"

	obsevents "github.com/andreykaipov/goobs/api/events"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeToggleFilter BindType = "toggle_filter"

type ToggleFilterBinding struct {
	BindingBase
	SourceName string `yaml:"sourceName"`
	FilterName string `yaml:"filterName"`
}

func init() {
	knownBindingType[TypeToggleFilter] = func(base BindingBase) Binding {
		return &ToggleFilterBinding{BindingBase: base}
	}
}

func (b *ToggleFilterBinding) Activate() {
	visible, err := b.obsClient.GetFilterState(b.SourceName, b.FilterName)
	if err != nil {
		slog.Warn("GetFilterState failed", logger.Error(err))
		return
	}

	stateName := StateNameOff
	if visible {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleFilterBinding) HandleOBS(event events.ObsEvent) {
	e, ok := event.Event.(*obsevents.SourceFilterEnableStateChanged)
	if !ok {
		return
	}

	if e.SourceName != b.SourceName || e.FilterName != b.FilterName {
		return
	}

	stateName := StateNameOff
	if e.FilterEnabled {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleFilterBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	_, err := b.obsClient.ToggleFilterState(b.SourceName, b.FilterName)
	if err != nil {
		slog.Warn("ToggleFilterState failed", logger.Error(err))
	}
}

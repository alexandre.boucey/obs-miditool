package page

import (
	"log/slog"

	obsevents "github.com/andreykaipov/goobs/api/events"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeSetCurrentScene BindType = "set_current_scene"

type SetCurrentSceneBinding struct {
	BindingBase
	SceneName string `yaml:"sceneName"`
}

func init() {
	knownBindingType[TypeSetCurrentScene] = func(base BindingBase) Binding {
		return &SetCurrentSceneBinding{BindingBase: base}
	}
}

func (b *SetCurrentSceneBinding) Activate() {
	sceneName, err := b.obsClient.GetCurrentProgramScene()
	if err != nil {
		slog.Warn("GetCurrentProgramScene failed", logger.Error(err))
		return
	}

	stateName := StateNameOff
	if b.SceneName == sceneName {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *SetCurrentSceneBinding) HandleOBS(event events.ObsEvent) {
	e, ok := event.Event.(*obsevents.CurrentProgramSceneChanged)
	if !ok {
		return
	}

	stateName := StateNameOff
	if e.SceneName == b.SceneName {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *SetCurrentSceneBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	_, err := b.obsClient.SetCurrentProgramScene(b.SceneName)
	if err != nil {
		slog.Warn("SetCurrentProgramScene failed", slog.String("scene_name", b.SceneName), logger.Error(err))
	}
}

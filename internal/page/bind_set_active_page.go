package page

import (
	"log/slog"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeSetActivePage BindType = "set_active_page"

type SetActivePageBinding struct {
	BindingBase
	PageID string `yaml:"page"`
}

func init() {
	knownBindingType[TypeSetActivePage] = func(base BindingBase) Binding {
		return &SetActivePageBinding{BindingBase: base}
	}
}

func (b *SetActivePageBinding) Activate() {
	stateName := StateNameOff
	if b.PageID == b.pageManager.GetActivePageID() {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *SetActivePageBinding) HandleOBS(event events.ObsEvent) {
	//No-op
}

func (b *SetActivePageBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	err := b.pageManager.SetActivePage(b.PageID)
	if err != nil {
		slog.Warn("SetActivePage failed", slog.String("page_key", b.PageID), logger.Error(err))
	}
}

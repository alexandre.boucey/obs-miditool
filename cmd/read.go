package cmd

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/spf13/cobra"

	"gitlab.com/gomidi/midi/v2"
	"gitlab.com/gomidi/midi/v2/drivers"
)

func init() {
	rootCmd.AddCommand(readCmd)
}

var readCmd = &cobra.Command{
	Use:   "read",
	Short: "Read midi inputs",
	Long:  `List all midi devices and read commands to help you identify the controls`,
	Run: func(cmd *cobra.Command, args []string) {
		defer midi.CloseDriver()
		inputs, err := drivers.Ins()
		if err != nil {
			log.Printf("Cannot list input ports: %s\n", err)
			return
		}
		log.Printf("Got %d inputs:\n", len(inputs))
		for _, baseName := range inputs {
			log.Printf("- %+v\n", baseName)
		}

		outputs, err := drivers.Outs()
		if err != nil {
			log.Printf("Cannot list output ports: %s\n", err)
			return
		}
		log.Printf("Got %d outputs:\n", len(outputs))
		for _, baseName := range outputs {
			log.Printf("- %+v\n", baseName)
		}

		if len(inputs) == 0 {
			log.Fatalln("No Midi input devices found")
		}

		log.Println("Opening all MIDI inputs")
		log.Println("Press CTRL-C to quit")

		c := make(chan os.Signal, 2)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)
		var stopFns []func()

		for _, in := range inputs {
			if strings.Contains(in.String(), "Through") {
				continue
			}

			func(in drivers.In) {
				stopFn, err := midi.ListenTo(in, func(msg midi.Message, timestampms int32) {
					log.Printf("[%s]: %+v\n", in.String(), msg)
				})
				if err != nil {
					log.Printf("Cannot open %s: %s\n", in.String(), err)
					return
				}

				stopFns = append(stopFns, stopFn)
			}(in)
		}

		<-c

		for _, fn := range stopFns {
			fn()
		}
	},
}

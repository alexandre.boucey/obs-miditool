package config

type Page struct {
	Name    string     `yaml:"name"`
	Include string     `yaml:"include"`
	Binds   []PageBind `yaml:"binds"`
}

type PageBind struct {
	Type     string                 `yaml:"type"`
	DeviceID string                 `yaml:"device"`
	InputID  string                 `yaml:"input"`
	Raw      map[string]interface{} `yaml:"-,inline"`
}

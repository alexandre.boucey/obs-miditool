package main

import (
	"os"
	"time"

	"github.com/tc-hib/winres"
	"github.com/tc-hib/winres/version"

	"midilink/internal/updater"
)

func main() {
	var err error
	rs := winres.ResourceSet{}

	manifest := winres.AppManifest{
		Identity:                          winres.AssemblyIdentity{},
		Description:                       "midilink",
		Compatibility:                     winres.Win7AndAbove,
		ExecutionLevel:                    winres.AsInvoker,
		UIAccess:                          false,
		AutoElevate:                       false,
		DPIAwareness:                      winres.DPIAware,
		DisableTheming:                    false,
		DisableWindowFiltering:            false,
		HighResolutionScrollingAware:      false,
		UltraHighResolutionScrollingAware: false,
		LongPathAware:                     false,
		PrinterDriverIsolation:            false,
		GDIScaling:                        false,
		SegmentHeap:                       false,
		UseCommonControlsV6:               false,
	}

	rs.SetManifest(manifest)

	versionInfo := version.Info{
		Timestamp: time.Now(),
	}
	versionInfo.SetFileVersion(updater.ReleaseVersionStringOrEmpty())
	versionInfo.SetProductVersion(updater.ReleaseVersionStringOrEmpty())
	versionInfo.Flags.Debug = !updater.IsReleaseVersion()

	err = versionInfo.Set(version.LangNeutral, version.ProductName, "midilink")
	if err != nil {
		panic(err)
	}

	rs.SetVersionInfo(versionInfo)

	byteReader, err := os.Open("internal/static/icon.ico")
	if err != nil {
		panic(err)
	}
	icon, err := winres.LoadICO(byteReader)
	if err != nil {
		panic(err)
	}

	err = rs.SetIcon(winres.RT_ICON, icon)
	if err != nil {
		panic(err)
	}

	out, _ := os.Create("rsrc_windows_amd64.syso")
	err = rs.WriteObject(out, winres.ArchAMD64)
	if err != nil {
		panic(err)
	}
}

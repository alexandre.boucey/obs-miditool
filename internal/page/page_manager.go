package page

import (
	"context"
	"fmt"
	"log/slog"
	"sync"

	"midilink/internal/config"
	"midilink/internal/device"
	"midilink/internal/events"
	"midilink/internal/obs"
)

const (
	KeyHome   = "_home_"
	KeyGlobal = "_global_"
)

type Manager struct {
	pages      map[string]*Page
	activePage *Page
	m          sync.RWMutex

	obsClient         *obs.ClientWrapper
	midiDeviceManager *device.Manager

	activePageNameChan chan string
}

func NewPageManager(obsClient *obs.ClientWrapper, deviceManager *device.Manager) (*Manager, error) {
	return &Manager{
		pages:              make(map[string]*Page),
		obsClient:          obsClient,
		midiDeviceManager:  deviceManager,
		activePageNameChan: make(chan string, 1),
	}, nil
}

func (p *Manager) DispatchEvent(event events.Event) {
	p.m.RLock()
	activePage := p.activePage
	p.m.RUnlock()

	if activePage == nil {
		return
	}

	activePage.Handle(event)
}

func (p *Manager) ActivePageName() <-chan string {
	return p.activePageNameChan
}

func (p *Manager) GetActivePageID() string {
	return p.activePage.ID()
}

func (p *Manager) SetActivePage(key string) error {
	p.m.Lock()
	defer p.m.Unlock()

	page, ok := p.pages[key]
	if !ok {
		return fmt.Errorf("no page defined for key=%s", key)
	}

	if p.activePage == page {
		return nil
	}

	if p.activePage != nil {
		p.activePage.Deactivate()
	} else {
		// pre-deactivate new page
		page.Deactivate()
	}

	p.activePage = page
	slog.Debug("Switching page", slog.String("page_name", page.Name()), slog.String("page_key", page.ID()))

	page.Activate()

	p.pushEvent()

	return nil
}

func (p *Manager) pushEvent() {
	select {
	case p.activePageNameChan <- p.activePage.Name():
	default:
		if len(p.activePageNameChan) == cap(p.activePageNameChan) {
			select {
			case <-p.activePageNameChan:
			default:
			}

			p.activePageNameChan <- p.activePage.Name()
		}
	}
}

func (p *Manager) Load(cfg map[string]config.Page) error {
	p.m.Lock()
	defer p.m.Unlock()

	hasHomePage := false
	p.pages = make(map[string]*Page, len(cfg))
	allPages := make(map[string]*Page, len(cfg))
	for pageKey, pageCfg := range cfg {
		page, err := NewPage(pageKey, pageCfg, p.obsClient, p.midiDeviceManager, p)
		if err != nil {
			return err
		}

		switch pageKey {
		case KeyGlobal:
			//TODO: decide how to handle global page
			//p.globalPage = page
			return fmt.Errorf("%s is a reserved page id", pageKey)
		case KeyHome:
			hasHomePage = true
			allPages[pageKey] = page
		default:
			allPages[pageKey] = page
		}

		slog.LogAttrs(context.TODO(), slog.LevelInfo, "Initialized new page", slog.String("page_name", page.Name()), slog.String("page_key", page.ID()))
	}

	if !hasHomePage {
		return fmt.Errorf("missing required home page with key %s", KeyHome)
	}

	// re-iterate over page array to handle extensions
	for pageKey, pageCfg := range cfg {
		if pageKey[0] == '.' {
			continue
		}

		current, ok := allPages[pageKey]
		if !ok {
			return fmt.Errorf("page key=%s not found on second iteration", pageKey)
		}

		p.pages[pageKey] = current

		if pageCfg.Include == "" {
			continue
		}

		base, ok := allPages[pageCfg.Include]
		if !ok {
			return fmt.Errorf("cannot find included page key=%s for page name=%s key=%s", pageCfg.Include, current.Name(), current.ID())

		}

		slog.LogAttrs(context.TODO(), slog.LevelInfo, "Merging binds", slog.String("current", current.ID()), slog.String("other", base.ID()))
		current.MergeWith(base)
	}

	return nil
}

func (p *Manager) Close() {
	p.m.Lock()
	defer p.m.Unlock()

	if p.activePage == nil {
		return
	}

	p.activePage.Deactivate()
	p.activePage = nil
	close(p.activePageNameChan)
}

package page

import (
	"log/slog"
	"sync"
	"time"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeEmitBrowserEvent BindType = "emit_browser_event"

type EmitBrowserEventBinding struct {
	BindingBase
	EventName string
	EventData map[string]interface{} `mapstructure:"eventData"`

	blinkTimer *time.Timer
	mu         sync.Mutex
}

func init() {
	knownBindingType[TypeEmitBrowserEvent] = func(base BindingBase) Binding {
		return &EmitBrowserEventBinding{BindingBase: base}
	}
}

func (b *EmitBrowserEventBinding) Deactivate() {
	b.BindingBase.Deactivate()

	b.mu.Lock()
	defer b.mu.Unlock()
	if b.blinkTimer != nil {
		b.blinkTimer.Stop()
		b.blinkTimer = nil
	}
}

func (b *EmitBrowserEventBinding) Activate() {
	b.setButtonState(StateNameOff)
}

func (b *EmitBrowserEventBinding) HandleOBS(event events.ObsEvent) {
	//No-op
}

func (b *EmitBrowserEventBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	_, err := b.obsClient.EmitBrowserEvent(b.EventName, b.EventData)
	if err != nil {
		slog.Warn("EmitBrowserEvent failed", slog.String("event_name", b.EventName), logger.Error(err))
		return
	}

	b.mu.Lock()
	defer b.mu.Unlock()
	if b.blinkTimer != nil {
		b.blinkTimer.Stop()
		b.blinkTimer = nil
	}

	b.setButtonState(StateNameOn)
	b.blinkTimer = time.AfterFunc(BlinkDuration, func() {
		b.setButtonState(StateNameOff)
	})
}

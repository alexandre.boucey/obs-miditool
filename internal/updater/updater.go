package updater

import (
	"fmt"
	"runtime"
	"sync"

	goversion "github.com/hashicorp/go-version"
	"github.com/mouuff/go-rocket-update/pkg/updater"
)

type Updater interface {
	GetLatestVersion() (*goversion.Version, bool, error)
	Update() (updater.UpdateStatus, error)
}

type semverUpdater struct {
	updater       updater.Updater
	version       *goversion.Version
	latestVersion *goversion.Version
	mu            sync.Mutex
}

var _ Updater = (*semverUpdater)(nil)

func New(prerelease bool) (Updater, error) {
	v, err := goversion.NewSemver(version)
	if err != nil {
		return nil, err
	}

	return &semverUpdater{
		updater: updater.Updater{
			Provider: &GitlabDirect{
				Project:     gitlabProject,
				ArchiveName: fmt.Sprintf("midilink_%s_%s", runtime.GOOS, runtime.GOARCH),
				Prerelease:  prerelease,
			},
			ExecutableName: fmt.Sprintf("midilink_%s_%s", runtime.GOOS, runtime.GOARCH),
		},
		version: v,
	}, nil
}

func (u *semverUpdater) GetLatestVersion() (*goversion.Version, bool, error) {
	if !IsReleaseVersion() {
		return &goversion.Version{}, false, nil
	}

	if u.latestVersion == nil {
		l, err := u.updater.GetLatestVersion()
		if err != nil {
			return nil, false, err
		}

		v, err := goversion.NewSemver(l)
		if err != nil {
			return nil, false, err
		}

		u.latestVersion = v
	}

	return u.latestVersion, u.latestVersion.GreaterThan(u.version), nil

}

func (u *semverUpdater) Update() (updater.UpdateStatus, error) {
	u.mu.Lock()
	defer u.mu.Unlock()

	latestVersion, canUpdate, err := u.GetLatestVersion()
	if err != nil {
		return updater.Unknown, err
	}

	// force the update by setting an incorrect version name
	if canUpdate {
		u.updater.Version = "this-is-not-a-real-version"
	} else {
		u.updater.Version = latestVersion.String()
	}

	return u.updater.Update()
}

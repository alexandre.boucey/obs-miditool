package page

import (
	"math"

	"midilink/internal/events"
)

type curveFunc func(float64) float64

const TypeVolume BindType = "volume"

type VolumeBinding struct {
	BindingBase
	SourceName string `yaml:"sourceName"`
	Mode       string `yaml:"mode"`
	c          curveFunc
}

func init() {
	knownBindingType[TypeVolume] = func(base BindingBase) Binding {
		return &VolumeBinding{BindingBase: base}
	}
}

const (
	curveModeLog    = "log"
	curveModeIec    = "iec"
	curveModeCubic  = "cubic"
	curveModeLinear = "linear"

	negInf = -100.0

	logOffsetDb = 6.0
	logRangeDb  = 96.0
)

func curveLog(mul float64) float64 {
	return -(logRangeDb+logOffsetDb)*math.Pow((logRangeDb+logOffsetDb)/logOffsetDb, -mul) + logOffsetDb
}

func curveIec(mul float64) float64 {
	switch {
	case mul >= 0.75:
		return (mul - 1.0) / 0.25 * 9.0
	case mul >= 0.5:
		return (mul-0.75)/0.25*11.0 - 9.0
	case mul >= 0.3:
		return (mul-0.5)/0.2*10.0 - 20.0
	case mul >= 0.15:
		return (mul-0.3)/0.15*10.0 - 30.0
	case mul >= 0.075:
		return (mul-0.15)/0.075*10.0 - 40.0
	case mul >= 0.025:
		return (mul-0.075)/0.05*10.0 - 50.0
	case mul >= 0.001:
		return (mul-0.025)/0.025*90.0 - 60.0
	}

	return negInf
}

func curveCubic(mul float64) float64 {
	return 20.0 * math.Log10(mul*mul*mul)
}

func curveLinear(mul float64) float64 {
	return (1 - mul) * (negInf)
}

func (b *VolumeBinding) Activate() {
	// load curve function on activation
	switch b.Mode {
	case curveModeLinear: // not recommended
		b.c = curveLinear
	case curveModeCubic:
		b.c = curveCubic
	case curveModeIec:
		b.c = curveIec
	default: // aka. "log"
		b.c = curveLog
	}
}

func (b *VolumeBinding) HandleOBS(event events.ObsEvent) {
	//No-op
}

func (b *VolumeBinding) HandleMIDI(event events.MidiEvent) {
	var ch, c, v uint8
	if !event.Message.GetControlChange(&ch, &c, &v) {
		return
	}

	m := float64(v) / 127.0
	switch {
	case m >= 1.0:
		m = 0.0
	case m <= 0.0:
		m = negInf
	default:
		if b.c == nil {
			return
		}

		m = b.c(m)
	}

	// logging error here might be very noisy
	_, _ = b.obsClient.SetVolumeDb(b.SourceName, m)
}

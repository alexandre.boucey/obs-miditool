package device

import (
	"midilink/internal/config"
	"midilink/internal/events"
)

// midiEventChanSize is the length of the buffered channel for all midi inputs.
// if the buffer is too large, when there is an influx of event there might be a process delay
// if too small, some event might be dropped
const midiEventChanSize = 10

type Manager struct {
	devices map[string]*Device

	events chan events.MidiEvent
}

func NewManager(cfg config.Midi, debug bool) (*Manager, error) {
	deviceManager := &Manager{
		devices: make(map[string]*Device),
		events:  make(chan events.MidiEvent, midiEventChanSize),
	}
	for s, deviceCfg := range cfg.Devices {
		dev, err := newDevice(s, deviceCfg.InPortName, deviceCfg.OutPortName, deviceCfg.Inputs, deviceManager)
		if err != nil {
			return nil, err
		}

		deviceManager.devices[s] = dev
	}

	return deviceManager, nil
}

func (d *Manager) Devices() map[string]*Device {
	return d.devices
}

func (d *Manager) GetDevice(deviceID string) *Device {
	return d.devices[deviceID]
}

func (d *Manager) pushEvent(e events.MidiEvent) {
	select {
	case d.events <- e:
	default:
		if len(d.events) == cap(d.events) {
			select {
			case <-d.events:
			default:
			}

			d.events <- e
		}
	}
}

func (d *Manager) Events() <-chan events.MidiEvent {
	return d.events
}

func (d *Manager) Close() {
	for _, device := range d.devices {
		device.Close()
	}
}

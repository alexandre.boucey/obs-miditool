package device

import (
	"errors"
	"fmt"
	"strconv"
	"sync"

	"gitlab.com/gomidi/midi/v2"
	"gitlab.com/gomidi/midi/v2/drivers"

	"midilink/internal/config"
	"midilink/internal/events"
)

var ErrDevicePortNotFound = errors.New("device port not found")

type Device struct {
	id     string
	sendFn func(msg midi.Message) error
	stopFn func()

	manager *Manager
	inPort  drivers.In
	outPort drivers.Out

	inputs map[inputIndex]*deviceInput
	states map[string]map[string]setStateAction
	m      sync.RWMutex
}

type deviceInput struct {
	input config.DeviceInput
	name  string
}

type inputIndex interface {
	GetChannel() uint8
}

type midiKeyInputIndex struct {
	channel uint8
	key     uint8
}

func (m midiKeyInputIndex) GetChannel() uint8 {
	return m.channel
}

type midiControlInputIndex struct {
	channel    uint8
	controller uint8
}

func (m midiControlInputIndex) GetChannel() uint8 {
	return m.channel
}

func newDevice(ID string, inPortName string, outPortName string, buttonProfile map[string]config.DeviceInput, man *Manager) (*Device, error) {
	inPort, err := midi.FindInPort(inPortName)
	if err != nil {
		return nil, fmt.Errorf("%w: %w", ErrDevicePortNotFound, err)
	}

	outPort, err := midi.FindOutPort(outPortName)
	if err != nil {
		return nil, fmt.Errorf("%w: %w", ErrDevicePortNotFound, err)
	}

	d := &Device{
		id:      ID,
		sendFn:  nil,
		stopFn:  nil,
		inPort:  inPort,
		outPort: outPort,
		manager: man,
		inputs:  make(map[inputIndex]*deviceInput),
		states:  make(map[string]map[string]setStateAction),
	}

	for inputName, inputDef := range buttonProfile {
		var k inputIndex
		switch input := inputDef.(type) {
		case *config.DeviceInputButtonWithState:
			k = midiKeyInputIndex{
				channel: input.Channel,
				key:     input.Key,
			}

			// States are stored in a separate map
			states := make(map[string]setStateAction)
			for s, actionDef := range input.States {
				var a setStateAction

				var key uint8
				keyStr, ok := actionDef.Args["key"]
				keyConv, err := strconv.ParseInt(keyStr, 10, 8)
				if ok && err == nil {
					key = uint8(keyConv)
				} else {
					key = input.Key
				}

				switch actionDef.Action {
				case "note_on":
					var velocity uint8
					velocityStr, ok := actionDef.Args["velocity"]
					velocityConv, err := strconv.ParseInt(velocityStr, 10, 8)
					if ok && err == nil {
						velocity = uint8(velocityConv)
					} else {
						velocity = 127
					}

					a = actionNoteOn{
						key:      key,
						velocity: velocity,
					}
				case "note_off":
					a = actionNoteOff{
						key: key,
					}
				default:
					continue
				}
				states[s] = a
			}

			d.states[inputName] = states

		case *config.DeviceInputButton:
			k = midiKeyInputIndex{
				channel: input.Channel,
				key:     input.Key,
			}
		case *config.DeviceInputController:
			k = midiControlInputIndex{
				channel:    input.Channel,
				controller: input.Controller,
			}
		default:
			continue
		}

		d.inputs[k] = &deviceInput{
			input: inputDef,
			name:  inputName,
		}
	}

	d.sendFn, err = midi.SendTo(outPort)
	if err != nil {
		return nil, err
	}
	d.stopFn, err = midi.ListenTo(inPort, d.processInputMessage)
	if err != nil {
		return nil, err
	}

	err = d.reset()
	if err != nil {
		return nil, err
	}

	return d, nil
}

func (d *Device) processInputMessage(msg midi.Message, _ int32) {
	var k inputIndex
	var ch, key, v uint8

	switch {
	case msg.GetControlChange(&ch, &key, &v):
		k = midiControlInputIndex{
			channel:    ch,
			controller: key,
		}
	case msg.GetNoteOn(&ch, &key, &v):
		k = midiKeyInputIndex{
			channel: ch,
			key:     key,
		}
	default:
		// ignore
	}

	input, ok := d.inputs[k]
	if !ok {
		return
	}

	e := events.MidiEvent{
		Message:  msg,
		DeviceID: d.id,
		InputID:  input.name,
		Input:    input.input,
	}

	d.manager.pushEvent(e)
}

func (d *Device) SetButtonState(buttonID string, stateID string) error {
	d.m.RLock()
	defer d.m.RUnlock()

	b, ok := d.states[buttonID]
	if !ok {
		return fmt.Errorf("button %s does not exist", buttonID)
	}

	s, ok := b[stateID]
	if !ok {
		return fmt.Errorf("button %s has no state %s", buttonID, stateID)
	}

	return d.sendFn(s.Message())
}

func (d *Device) reset() error {
	var err error
	for _, msg := range midi.SilenceChannel(-1) {
		err = d.sendFn(msg)
		if err != nil {
			return err
		}
	}
	var i uint8
	var c uint8
	for c = 0; c < 16; c++ {
		for i = 0; i < 128; i++ {
			err = d.sendFn(midi.NoteOn(c, i, 0))
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (d *Device) Close() {
	_ = d.reset()
	d.stopFn()
	_ = d.inPort.Close()
	_ = d.outPort.Close()
}

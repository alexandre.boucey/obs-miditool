# midilink | _Control OBS from a midi device_
    
Use your midi controller to trigger scene change, edit visibility, volumes and filters directly on OBS. You can also use it to trigger actions on [Streamer.bot](https://streamer.bot/) and in the obs browser source. 

## Why it's not an OBS plugin
In your livestream or recording, OBS is already doing a lot of things. Adding a plugin is adding a risk of unexpected crash and more load to the program.    
If there is an issue with midilink, you just loose the control surface and not your whole stream. Also, it's easier to update a standalone program that doesn't depend on a specific OBS version.

## Requirements
### MIDI
Any midi controller can work (for example the [AKAI APC mini](https://www.akaipro.com/apc-mini)). You can also use multiple controller at once, by using different devices or midi channels

### obs-websocket
Starting with OBS 28, the obs-websocket is included. But for versions before 28, you have to install manually [obs-websocket](https://github.com/obsproject/obs-websocket) version 5.
> Older protocol of obs-websocket (like the `-compat` or before 5.0.0) are not supported

## Features
- Use sliders or potentiometers to control input volumes
- Define mapping for oneshot actions such as triggering Streamer.bot or obs-browser javascript events _(see below for an example)_
- Scene changes are synced two-ways between the controller and OBS. If your controller support it, changing any mapped actions like scene transition, filter states, etc.. in OBS will change the corresponding light on the controller.
- Configuration using a single YAML file

## Getting started
Use the provided `config.example.yaml` as a getting started. Copy it as `config.yaml` and edit the mapping as you need.
The file is only read at program startup, you have to restart the app to apply configuration change.

## Planned features and improvements
- Create a database of pre-filled mappings for various controllers
- Live configuration reload when the file is modified
- UI to provide easy configuration and state visualization

<details>
<summary>Example of browser source javascript event</summary>

### Browser source javascript event
In the YAML configuration, define your mapping for the action type `emit_browser_event`

```yml
      - type: emit_browser_event
        device: my-device
        input: rec5
        eventName: 'my_custom_event_name'
        eventData:
          hello: 'world'
          more_data: true
```

In the webpage of the browser source:
```javascript
window.addEventListener('my_custom_event_name', function(event) {
    console.log(event.detail)
})
```
</details>

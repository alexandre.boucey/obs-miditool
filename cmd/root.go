package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/getlantern/systray"
	"github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	"github.com/sqweek/dialog"

	"midilink/internal/app"
	"midilink/internal/config"
	"midilink/internal/driver"
	"midilink/internal/logger"
	"midilink/internal/static"
	"midilink/internal/updater"
)

const mLastErrorMessageDefaultText = "No errors"

var controller *app.App

var mActivePageName *systray.MenuItem
var mLastErrorMessage *systray.MenuItem
var mQuit *systray.MenuItem

var closeOnce = sync.OnceFunc(func() {
	if controller != nil {
		controller.Stop()
		// do not nil the controller here, there is a data race
		// we are exiting the app, so it doesn't matter for now
	}

	systray.Quit()
})

func onSystrayReady() {
	defer sentry.Recover()

	systray.SetIcon(static.Icon)
	systray.SetTitle("")
	systray.SetTooltip(fmt.Sprintf("Midilink v%s - RtMidi v%s", updater.VersionString(), driver.GetVersion()))
	mTitle := systray.AddMenuItem("Midilink "+updater.VersionString(), "")
	mTitle.SetIcon(static.Icon)
	mTitle.Disable()

	mActivePageName = systray.AddMenuItem("Application is starting...", "")
	mActivePageName.Hide()
	mActivePageName.Disable()

	mLastErrorMessage = systray.AddMenuItem(mLastErrorMessageDefaultText, "")
	mLastErrorMessage.Hide()
	mLastErrorMessage.Disable()

	systray.AddSeparator()
	mQuit = systray.AddMenuItem("Quit", "Quit the whole app")
	go func() {
		<-mQuit.ClickedCh
		closeOnce()
	}()

	startApplication()
}

func startApplication() {
	defer closeOnce()

	cfg, err := config.Read(*configFile)
	if err != nil {
		merr := fmt.Errorf("config error: %w", err)
		handleError(merr)
		dialog.Message("Unable to load configuration: %s", merr).Title("Configuration error").Error()
		return
	}

	if cfg.Debug {
		logger.SetLevel(slog.LevelDebug)
	} else {
		logger.SetLevel(slog.LevelInfo)
	}

	func() {
		if cfg.Updater.Disable {
			return
		}

		u, err := updater.New(cfg.Updater.Prerelease)
		if err != nil {
			slog.Warn("cannot initialize updater", logger.Error(err))
		}

		v, canUpdate, err := u.GetLatestVersion()
		if err != nil {
			slog.Warn("update check failed", logger.Error(err))
		}

		if canUpdate {
			slog.Info("A new version is available", "version", v)

			dialog.
				Message(
					"A new version of Midilink is available: v%s",
					v.String(),
				).
				Title("New version available").
				Info()
		}
	}()

	controller, err = app.FromConfig(cfg)
	if err != nil {
		merr := fmt.Errorf("app init error: %w", err)
		handleError(merr)
		return
	}

	go controller.Loop()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	for {
		select {
		case p := <-controller.ActivePageName():
			mActivePageName.SetTitle("Active page: " + p)
			mActivePageName.Show()

		case e := <-controller.Errors():
			if e == nil { // special case, just clear error
				mLastErrorMessage.SetTitle(mLastErrorMessageDefaultText)
				mLastErrorMessage.Hide()
			} else {
				handleError(e)
			}

		case <-controller.StopChan():
			closeOnce()
			return

		case <-c:
			closeOnce()
			return
		}
	}
}

func handleError(err error) {
	sentry.CaptureException(err)
	slog.Warn("caught an exception", logger.Error(err))
	mLastErrorMessage.SetTitle(fmt.Sprintf("%s", err))
	mLastErrorMessage.Show()
}

var configFile *string = nil

var rootCmd = &cobra.Command{
	Use:   "midilink",
	Short: "Midilink is the missing link between MIDI and OBS",
	Long: `Use your midi devices with OBS for transitions,
                 volumes and actions.`,
	Run: func(cmd *cobra.Command, args []string) {
		systray.Run(onSystrayReady, nil)
	},
}

func init() {
	configFile = rootCmd.Flags().String("config", "config.yaml", "configuration file to load")
}

func Execute() error {
	return rootCmd.Execute()
}

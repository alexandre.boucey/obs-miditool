# Configuration
_Version 1_

The application is configurable using the `config.yaml` file located next to the executable.
The file is never written by the application to prevent overwriting users comments.

The file is not hot-reloaded, to update the state you have to restart the application

## Example config file
```yaml
---
version: 1
debug: false
updater:
  disable: false
  prerelease: true
obs:
  url: "127.0.0.1:4440"
  password: "abcd"

midi:
  # Midi driver, for now only rtmidi is supported
  driver: "rtmidi"

  # Declare your midi devices here
  devices:
    midimix:
      in: "MIDI Mix MIDI 1"
      out: "MIDI Mix MIDI 1"

      # Inputs map physical buttons to user-friendly names
      # You can define them inside this config file and/or import an external file

      # If left empty or not specified, no profile is loaded
      profileFile: "./doc/devices/akai_midimix.yaml"

      # If an input is declared both in the profile file and this config file, the one in this config file is used.
      inputs:
        bank_left:
          type: button
          channel: 0 # zero by default if not specified
          key: 25
          states:
            on: # "on" state (the related trigger reports true)
              action: note_on
              velocity: 127 # default to 127 if not specified
            off: # "off" state (the related trigger reports false)
              action: note_on
              velocity: 0
            unassigned: # "unassigned" state. The default state of the button before the trigger reports status
              action: note_off # some devices (including the MIDIMix) doesn't handle note_off, only note_on with velocity = 0

# At least one page, with key "_home_", is required
pages:
  # page key starting wit a dot are ignored.
  # use them as `include` in other page
  .commons:
    binds:
      - type: set_active_page
        device: midimix
        input: bank_left
        PageID: '_home_'

      - type: set_active_page
        device: midimix
        input: bank_right
        PageID: 'scene-switch'

      - type: volume
        device: midimix
        input: vol1
        mode: log # curve: log (obs default style) / iec (IEC 60-268-18) / cubic (approx), default to log if not set
        sourceName: 'pc-audio'

      - type: toggle_mute
        device: midimix
        input: mute1
        sourceName: 'pc-audio'

      - type: volume
        device: midimix
        input: vol2
        sourceName: 'mic-audio'

      - type: toggle_mute
        device: midimix
        input: mute2
        sourceName: 'mic-audio'

  # Home page
  _home_:
    include: .commons # optional - set the page ID to include
    name: "Home"

    binds:
      - type: toggle_source
        device: midimix
        input: rec1
        sourceName: 'color-cube'
        sceneName: 'COLOR'

      - type: toggle_filter
        device: midimix
        input: rec2
        sourceName: 'color-cube'
        filterName: 'corr'

      - type: execute_streamerbot_action
        device: midimix
        input: rec3
        url: 'http://127.0.0.1:7474/DoAction' # default to this value if not specified
        # either actionId or actionName should be defined
        # if both are defined, actionId is used
        actionId: '36264db0-8cb0-42eb-8d20-d96c5a6a7de1'
        actionName: 'my_action'
        args: # args for the action
          hello: "world"

      - type: execute_streamerbot_action
        device: midimix
        input: rec4
        actionName: 'the_action_by_name'

      - type: emit_browser_event
        device: midimix
        input: rec5
        eventName: 'hello_world'
        eventData:
          test: 'oui'
          toto: 5

      - type: set_transform
        device: midimix
        input: rec6
        sourceName: 'color-cube'
        sceneName: 'COLOR'
        positionX: 500
        positionY: 250
        width: 100
        height: 100

      - type: set_transform
        device: midimix
        input: rec7
        sourceName: 'color-cube'
        sceneName: 'COLOR'
        positionX: 0
        positionY: 0
        width: 500
        height: 500


  # example of a second page
  scene-switch:
    include: .commons
    name: "Scenes"

    binds:
      - type: set_current_scene
        device: midimix
        input: rec1
        sceneName: 'RECSCREEN'

      - type: set_current_scene
        device: midimix
        input: rec2
        sceneName: 'COLOR'

      - type: set_current_scene
        device: midimix
        input: rec3
        sceneName: 'AFK'
```
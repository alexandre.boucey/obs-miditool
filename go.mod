module midilink

go 1.23

toolchain go1.23.2

replace gitlab.com/gomidi/midi/v2 v2.2.14 => gitlab.com/alexandre.boucey/midi/v2 v2.2.14-0.20241101151123-dcbe3874e761

require (
	github.com/andreykaipov/goobs v1.5.3
	github.com/getlantern/systray v1.2.2
	github.com/getsentry/sentry-go v0.29.1
	github.com/goccy/go-yaml v1.13.2
	github.com/hashicorp/go-version v1.7.0
	github.com/lmittmann/tint v1.0.5
	github.com/mitchellh/mapstructure v1.5.0
	github.com/mouuff/go-rocket-update v1.5.4
	github.com/samber/slog-multi v1.2.4
	github.com/spf13/cobra v1.8.1
	github.com/sqweek/dialog v0.0.0-20240226140203-065105509627
	github.com/tc-hib/winres v0.3.1
	gitlab.com/gomidi/midi/v2 v2.2.14
)

require (
	github.com/TheTitanrain/w32 v0.0.0-20180517000239-4f5cfb03fabf // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/fatih/color v1.18.0 // indirect
	github.com/getlantern/context v0.0.0-20220418194847-3d5e7a086201 // indirect
	github.com/getlantern/errors v1.0.3 // indirect
	github.com/getlantern/golog v0.0.0-20230503153817-8e72de7e0a65 // indirect
	github.com/getlantern/hex v0.0.0-20220104173244-ad7e4b9194dc // indirect
	github.com/getlantern/hidden v0.0.0-20220104173330-f221c5a24770 // indirect
	github.com/getlantern/ops v0.0.0-20231025133620-f368ab734534 // indirect
	github.com/go-logr/logr v1.3.0 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/hashicorp/logutils v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mmcloughlin/profile v0.1.1 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c // indirect
	github.com/samber/lo v1.47.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go.opentelemetry.io/otel v1.21.0 // indirect
	go.opentelemetry.io/otel/metric v1.21.0 // indirect
	go.opentelemetry.io/otel/trace v1.21.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
	golang.org/x/image v0.14.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.19.0 // indirect
)

package page

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log/slog"
	"net/http"
	"sync"
	"time"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeExecuteStreamerBotAction BindType = "execute_streamerbot_action"

var localHttpClient = http.Client{
	Timeout: 50 * time.Millisecond,
}

type ExecuteStreamerBotActionBinding struct {
	BindingBase
	URL        string `mapstructure:"url"`
	ActionID   string
	ActionName string
	Args       map[string]interface{}

	blinkTimer *time.Timer
	mu         sync.Mutex
}

func init() {
	knownBindingType[TypeExecuteStreamerBotAction] = func(base BindingBase) Binding {
		return &ExecuteStreamerBotActionBinding{BindingBase: base}
	}
}

func (b *ExecuteStreamerBotActionBinding) Deactivate() {
	b.BindingBase.Deactivate()

	b.mu.Lock()
	defer b.mu.Unlock()
	if b.blinkTimer != nil {
		b.blinkTimer.Stop()
		b.blinkTimer = nil
	}
}

func (b *ExecuteStreamerBotActionBinding) Activate() {
	b.setButtonState(StateNameOff)
}

func (b *ExecuteStreamerBotActionBinding) HandleOBS(event events.ObsEvent) {
	//No-op
}

func (b *ExecuteStreamerBotActionBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	targetUrl := b.URL
	if targetUrl == "" {
		targetUrl = "http://127.0.0.1:7474/DoAction"
	}

	type NameOrID struct {
		ID   string `json:"id,omitempty"`
		Name string `json:"name,omitempty"`
	}

	body := map[string]interface{}{
		"action": NameOrID{
			b.ActionID,
			b.ActionName,
		},
		"args": b.Args,
	}

	jsonValue, err := json.Marshal(body)
	if err != nil {
		slog.Error("Failed to encode Streamer.bot request", logger.Error(err))
		return
	}
	req, err := http.NewRequestWithContext(context.TODO(), http.MethodPost, targetUrl, bytes.NewBuffer(jsonValue))
	if err != nil {
		slog.Error("Request to Streamer.bot failed", logger.Error(err))
		return
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := localHttpClient.Do(req)
	if err != nil {
		slog.Error("Request to Streamer.bot failed", logger.Error(err))
		return
	}

	if resp.StatusCode != http.StatusNoContent {
		defer resp.Body.Close()
		bodyStr, _ := io.ReadAll(resp.Body)
		slog.Warn("Bad response from Streamer.bot", slog.String("status", resp.Status), slog.String("body", string(bodyStr)))
		return
	}

	b.mu.Lock()
	defer b.mu.Unlock()
	if b.blinkTimer != nil {
		b.blinkTimer.Stop()
		b.blinkTimer = nil
	}

	b.setButtonState(StateNameOn)
	b.blinkTimer = time.AfterFunc(BlinkDuration, func() {
		b.setButtonState(StateNameOff)
	})
}

package obs

import (
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/andreykaipov/goobs"
	goobsSubscriptions "github.com/andreykaipov/goobs/api/events/subscriptions"
	"github.com/andreykaipov/goobs/api/requests/filters"
	"github.com/andreykaipov/goobs/api/requests/general"
	"github.com/andreykaipov/goobs/api/requests/inputs"
	"github.com/andreykaipov/goobs/api/requests/sceneitems"
	"github.com/andreykaipov/goobs/api/requests/scenes"
	"github.com/andreykaipov/goobs/api/typedefs"
)

// subscriptions is the list of high volume event we want to handle
const subscriptions = goobsSubscriptions.All |
	goobsSubscriptions.InputActiveStateChanged |
	goobsSubscriptions.InputShowStateChanged |
	goobsSubscriptions.SceneItemTransformChanged

var ErrWebsocketConnectionFailed = errors.New("websocket connection failed")

type ClientWrapper struct {
	con      *goobs.Client
	host     string
	password string
	debug    bool
}

func NewClientWrapper(host string, password string, debug bool) (*ClientWrapper, error) {
	return &ClientWrapper{
		con:      nil,
		host:     host,
		password: password,
		debug:    debug,
	}, nil
}

func (c *ClientWrapper) Connect() error {
	if c.con != nil {
		return nil
	}

	con, err := goobs.New(c.host,
		goobs.WithPassword(c.password),
		goobs.WithResponseTimeout(50*time.Millisecond),
		goobs.WithEventSubscriptions(subscriptions),
		goobs.WithLogger(slog.NewLogLogger(slog.Default().Handler(), slog.LevelDebug)),
	)
	if err != nil {
		return fmt.Errorf("%w: %w", ErrWebsocketConnectionFailed, err)
	}

	c.con = con
	return nil
}

func (c *ClientWrapper) IncomingEvents() <-chan interface{} {
	return c.con.IncomingEvents
}

func (c *ClientWrapper) GetFilterState(sourceName string, filterName string) (bool, error) {
	r, err := c.con.Filters.GetSourceFilter(&filters.GetSourceFilterParams{
		FilterName: &filterName,
		SourceName: &sourceName,
	})

	if err != nil {
		return false, err
	}

	return r.FilterEnabled, nil
}

func (c *ClientWrapper) ToggleFilterState(sourceName string, filterName string) (*filters.SetSourceFilterEnabledResponse, error) {
	r, err := c.GetFilterState(sourceName, filterName)

	if err != nil {
		return nil, err
	}

	invert := !r
	return c.con.Filters.SetSourceFilterEnabled(&filters.SetSourceFilterEnabledParams{
		FilterEnabled: &invert,
		SourceName:    &sourceName,
		FilterName:    &filterName,
	})
}

func (c *ClientWrapper) GetSceneItemId(sceneName string, sourceName string) (int, error) {
	r, err := c.con.SceneItems.GetSceneItemId(&sceneitems.GetSceneItemIdParams{
		SceneName:  &sceneName,
		SourceName: &sourceName,
	})

	if err != nil {
		return 0, err
	}

	return r.SceneItemId, nil
}

func (c *ClientWrapper) GetSceneItemEnabled(sceneName string, sceneItemId int) (bool, error) {
	r, err := c.con.SceneItems.GetSceneItemEnabled(&sceneitems.GetSceneItemEnabledParams{
		SceneItemId: &sceneItemId,
		SceneName:   &sceneName,
	})

	if err != nil {
		return false, err
	}

	return r.SceneItemEnabled, nil
}

func (c *ClientWrapper) GetSceneItemTransform(sceneName string, sceneItemId int) (*typedefs.SceneItemTransform, error) {
	r, err := c.con.SceneItems.GetSceneItemTransform(&sceneitems.GetSceneItemTransformParams{
		SceneItemId: &sceneItemId,
		SceneName:   &sceneName,
	})

	if err != nil {
		return nil, err
	}

	return r.SceneItemTransform, nil
}

func (c *ClientWrapper) SetSceneItemTransform(sceneName string, sceneItemId int, x float64, y float64, width float64, height float64) (*sceneitems.SetSceneItemTransformResponse, error) {
	transform, err := c.GetSceneItemTransform(sceneName, sceneItemId)
	if err != nil {
		return nil, err
	}

	scaleX := width / transform.SourceWidth
	scaleY := height / transform.SourceHeight

	transform.PositionX = x
	transform.PositionY = y
	transform.ScaleX = scaleX
	transform.ScaleY = scaleY

	// fix for default values being zero on new scene items but invalid
	if transform.BoundsType == "OBS_BOUNDS_NONE" {
		if transform.BoundsWidth == 0 {
			transform.BoundsWidth = 1
		}
		if transform.BoundsHeight == 0 {
			transform.BoundsHeight = 1
		}
	}

	return c.con.SceneItems.SetSceneItemTransform(&sceneitems.SetSceneItemTransformParams{
		SceneItemId:        &sceneItemId,
		SceneName:          &sceneName,
		SceneItemTransform: transform,
	})
}

func (c *ClientWrapper) ToggleSourceState(sceneName string, sceneItemId int) (*sceneitems.SetSceneItemEnabledResponse, error) {
	enabled, err := c.GetSceneItemEnabled(sceneName, sceneItemId)
	if err != nil {
		return nil, err
	}

	invert := !enabled
	return c.con.SceneItems.SetSceneItemEnabled(&sceneitems.SetSceneItemEnabledParams{
		SceneItemEnabled: &invert,
		SceneItemId:      &sceneItemId,
		SceneName:        &sceneName,
	})
}

func (c *ClientWrapper) SetVolumeDb(sourceName string, db float64) (*inputs.SetInputVolumeResponse, error) {
	params := inputs.SetInputVolumeParams{
		InputName:     &sourceName,
		InputVolumeDb: &db,
	}

	return c.con.Inputs.SetInputVolume(&params)
}

func (c *ClientWrapper) GetCurrentProgramScene() (string, error) {
	r, err := c.con.Scenes.GetCurrentProgramScene(&scenes.GetCurrentProgramSceneParams{})
	if err != nil {
		return "", err
	}

	return r.CurrentProgramSceneName, nil
}

func (c *ClientWrapper) SetCurrentProgramScene(sceneName string) (*scenes.SetCurrentProgramSceneResponse, error) {
	return c.con.Scenes.SetCurrentProgramScene(&scenes.SetCurrentProgramSceneParams{
		SceneName: &sceneName,
	})
}

func (c *ClientWrapper) GetInputMute(inputName string) (bool, error) {
	res, err := c.con.Inputs.GetInputMute(&inputs.GetInputMuteParams{
		InputName: &inputName,
	})
	if err != nil {
		return false, err
	}

	return res.InputMuted, nil
}

func (c *ClientWrapper) ToggleMuteState(inputName string) (bool, error) {
	res, err := c.con.Inputs.ToggleInputMute(&inputs.ToggleInputMuteParams{
		InputName: &inputName,
	})
	if err != nil {
		return false, err
	}

	return res.InputMuted, nil
}

var (
	obsBrowserVendorName = "obs-browser"
	emitEventRequestType = "emit_event"
)

func (c *ClientWrapper) EmitBrowserEvent(eventName string, eventData map[string]interface{}) (interface{}, error) {
	res, err := c.con.General.CallVendorRequest(&general.CallVendorRequestParams{
		VendorName:  &obsBrowserVendorName,
		RequestType: &emitEventRequestType,
		RequestData: map[string]interface{}{
			"event_name": eventName,
			"event_data": eventData,
		},
	})
	if err != nil {
		return nil, err
	}

	return res.ResponseData, nil
}

package page

import (
	"log/slog"

	"github.com/mitchellh/mapstructure"

	"midilink/internal/config"
	"midilink/internal/device"
	"midilink/internal/events"
	"midilink/internal/logger"
	"midilink/internal/obs"
)

type Page struct {
	id   string
	name string
	//events   map[string][]*EventHandler
	//actions  map[ActionKey]actions.Action
	bindings []Binding
}

func NewPage(key string, cfg config.Page, obsClient *obs.ClientWrapper, deviceManager *device.Manager, pageManager *Manager) (*Page, error) {
	page := &Page{
		id:   key,
		name: cfg.Name,
	}

	page.bindings = make([]Binding, 0)
	for _, bindCfg := range cfg.Binds {
		bindType := BindType(bindCfg.Type)
		b := NewBinding(
			bindType,
			bindCfg.DeviceID,
			bindCfg.InputID,
			obsClient,
			deviceManager,
			pageManager,
		)

		if b == nil {
			continue
		}

		if err := mapstructure.Decode(bindCfg.Raw, b); err != nil {
			slog.Warn("Failed to decode additionnal settings for the bind",
				slog.String("bind_type", string(bindType)),
				slog.Any("page_key", key),
				logger.Error(err),
			)
			continue
		}

		page.bindings = append(page.bindings, b)
	}

	return page, nil
}

func (p *Page) Name() string {
	return p.name
}

func (p *Page) ID() string {
	return p.id
}

func (p *Page) MergeWith(otherPage *Page) {
	if p == otherPage {
		return
	}

	p.bindings = append(p.bindings, otherPage.bindings...)
}

func (p *Page) Activate() {
	for _, binding := range p.bindings {
		binding.Activate()
	}
}

func (p *Page) Deactivate() {
	for _, binding := range p.bindings {
		binding.Deactivate()
	}
}

func (p *Page) Handle(event events.Event) {
	switch e := event.(type) {
	case events.MidiEvent:
		for _, b := range p.bindings {
			if b.GetDeviceID() != e.DeviceID || b.GetInputID() != e.InputID {
				continue
			}

			b.HandleMIDI(e)
		}
	case events.ObsEvent:
		for _, b := range p.bindings {
			b.HandleOBS(e)
		}
	}
}

package logger

import (
	"bufio"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/lmittmann/tint"
	slogmulti "github.com/samber/slog-multi"
	"github.com/sqweek/dialog"
)

var lvl = &slog.LevelVar{}

func SetLevel(l slog.Level) {
	lvl.Set(l)
}

func Error(err error) slog.Attr {
	return slog.Any("error", err)
}

// SetupLogger initialize the loggers
//
// The order is important. When we build for windows, os.Stderr and os.Stdout might be nil (GUI app)
// as slogmulti.Fanout stop when there is an error, each handler is checked before initalization
// Still we initialize the file logger first and then the console logger as a failsafe measure
func SetupLogger() (flushFunc func()) {
	// ensure flushFunc is never nil
	flushFunc = func() {
		// noop
	}
	var logHandlers []slog.Handler

	logFileName := fmt.Sprintf("output-%s.log", time.Now().Format("2006-01-02"))
	logFile, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err == nil {
		buf := bufio.NewWriter(logFile)
		flushFunc = func() {
			_ = buf.Flush()
			_ = logFile.Sync()
			_ = logFile.Close()
		}
		logHandlers = append(logHandlers, slog.NewTextHandler(buf, &slog.HandlerOptions{
			Level:     lvl,
			AddSource: true,
		}))
	} else {
		dialog.Message("Cannot create log: %s", err).Error()
	}

	w := os.Stderr
	if w != nil {
		logHandlers = append(logHandlers, tint.NewHandler(w, &tint.Options{
			Level:      lvl,
			TimeFormat: time.Kitchen,
		}))
	}

	if len(logHandlers) == 0 {
		dialog.Message("Empty log handlers. That's quite unexpected. Program will continue without proper logging.").Error()
		return
	}

	logger := slog.New(slogmulti.Fanout(logHandlers...))
	slog.SetDefault(logger)

	return
}

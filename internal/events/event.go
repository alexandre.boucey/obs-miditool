package events

import (
	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/config"
)

type Event interface {
}

type MidiEvent struct {
	Message  midi.Message
	DeviceID string
	InputID  string
	Input    config.DeviceInput
}

type ObsEvent struct {
	Event interface{}
}

package page

import (
	"log/slog"

	obsevents "github.com/andreykaipov/goobs/api/events"

	"gitlab.com/gomidi/midi/v2"

	"midilink/internal/events"
	"midilink/internal/logger"
)

const TypeToggleSource BindType = "toggle_source"

type ToggleSourceBinding struct {
	BindingBase
	SourceName string `yaml:"sourceName"`
	SceneName  string `yaml:"sceneName"`
}

func init() {
	knownBindingType[TypeToggleSource] = func(base BindingBase) Binding {
		return &ToggleSourceBinding{BindingBase: base}
	}
}

func (b *ToggleSourceBinding) Activate() {
	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	muted, err := b.obsClient.GetSceneItemEnabled(b.SceneName, sceneItemId)
	if err != nil {
		slog.Warn("GetSceneItemEnabled failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	stateName := StateNameOff
	if muted {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleSourceBinding) HandleOBS(event events.ObsEvent) {
	e, ok := event.Event.(*obsevents.SceneItemEnableStateChanged)
	if !ok {
		return
	}

	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	if e.SceneName != b.SceneName || e.SceneItemId != sceneItemId {
		return
	}

	stateName := StateNameOff
	if e.SceneItemEnabled {
		stateName = StateNameOn
	}

	b.setButtonState(stateName)
}

func (b *ToggleSourceBinding) HandleMIDI(event events.MidiEvent) {
	if !event.Message.Is(midi.NoteOnMsg) {
		return
	}

	sceneItemId, err := b.obsClient.GetSceneItemId(b.SceneName, b.SourceName)
	if err != nil {
		slog.Warn("GetSceneItemId failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
		return
	}

	_, err = b.obsClient.ToggleSourceState(b.SceneName, sceneItemId)
	if err != nil {
		slog.Warn("ToggleSourceState failed",
			slog.String("scene_name", b.SceneName),
			slog.String("source_name", b.SourceName),
			logger.Error(err),
		)
	}
}

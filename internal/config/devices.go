package config

type Device struct {
	InPortName  string         `yaml:"in"`
	OutPortName string         `yaml:"out"`
	Inputs      DeviceInputMap `yaml:"inputs"`
	ProfileFile string         `yaml:"profileFile"`
}

type DeviceProfile struct {
	DisplayName string         `yaml:"displayName"`
	Inputs      DeviceInputMap `yaml:"inputs"`
}

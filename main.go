package main

import (
	"log"
	"log/slog"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/cobra"
	"github.com/sqweek/dialog"

	_ "gitlab.com/gomidi/midi/v2/drivers/rtmididrv" //nolint:gci // force the uses of rtmidi globally

	"midilink/cmd"
	"midilink/internal/driver"
	"midilink/internal/logger"
	"midilink/internal/updater"
)

func main() {
	// allow launching by double-clicking on the exe
	cobra.MousetrapHelpText = ""

	flushFunc := logger.SetupLogger()

	slog.Info("Midilink", "version", updater.VersionString(), "rtmidi", driver.GetVersion())

	sentryEnv := "dev"
	if updater.IsReleaseVersion() {
		sentryEnv = "release"
	}

	sentry.Logger = slog.NewLogLogger(slog.Default().Handler(), slog.LevelDebug)
	err := sentry.Init(sentry.ClientOptions{
		Dsn:         "https://35bbf7a73e784c95a6f0683c516157f1@o512511.ingest.sentry.io/6188977",
		Release:     updater.ReleaseVersionStringOrEmpty(),
		Environment: sentryEnv,
		Debug:       false, // controlled by slog level
		IgnoreErrors: []string{
			"close 1001 (going away): Server stopping",
			"dial tcp",
			"device port not found",
		},
	})
	if err != nil {
		dialog.Message("Unable to start Sentry: %s", err).Title("Cannot initialize sentry").Error()
		log.Fatalf("sentry.Init: %s", err)
	}

	defer sentry.Recover()

	code := 0
	if err = cmd.Execute(); err != nil {
		slog.Error("cmd execute returned an error", logger.Error(err))
		code = 1
	}

	sentry.Flush(2 * time.Second)
	if flushFunc != nil {
		flushFunc()
	}

	os.Exit(code)
}

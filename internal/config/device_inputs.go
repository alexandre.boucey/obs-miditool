package config

import (
	"fmt"
	"strings"

	"github.com/goccy/go-yaml"
)

type DeviceInputType string

const (
	DeviceInputTypeButton     DeviceInputType = "button"
	DeviceInputTypeController DeviceInputType = "controller"
)

type DeviceInput interface {
	Type() DeviceInputType
}

type DeviceInputButton struct {
	Channel uint8 `yaml:"channel"`
	Key     uint8 `yaml:"key"`
}

type DeviceInputButtonWithState struct {
	*DeviceInputButton `yaml:",inline"`
	States             map[string]DeviceInputButtonState `yaml:"states"`
}

func (d DeviceInputButton) Type() DeviceInputType {
	return DeviceInputTypeButton
}

type DeviceInputButtonState struct {
	Action string            `yaml:"action"`
	Args   map[string]string `yaml:"-,inline"`
}

type DeviceInputController struct {
	Channel    uint8 `yaml:"channel"`
	Controller uint8 `yaml:"controller"`
}

func (d DeviceInputController) Type() DeviceInputType {
	return DeviceInputTypeController
}

// deviceInputBase store the unmarshaler for this DeviceInput for later
//
// We erase the type field with this, but we have the Type() func to normalize it
type deviceInputBase struct {
	Type      string `yaml:"type"`
	unmarshal func(interface{}) error
}

var _ yaml.InterfaceUnmarshaler = (*deviceInputBase)(nil)

func (d *deviceInputBase) UnmarshalYAML(f func(interface{}) error) error {
	var typeStruct struct {
		Type string `yaml:"type"`
	}
	if err := f(&typeStruct); err != nil {
		return err
	}

	d.Type = typeStruct.Type
	d.unmarshal = f

	return nil
}

type DeviceInputMap map[string]DeviceInput

var _ yaml.InterfaceUnmarshaler = (*DeviceInputMap)(nil)

// UnmarshalYAML decode the map with the correct custom type for each inputs
func (d *DeviceInputMap) UnmarshalYAML(f func(interface{}) error) error {
	var db map[string]deviceInputBase
	if err := f(&db); err != nil {
		return err
	}

	inputMap := make(DeviceInputMap, len(db))
	for s, i := range db {
		var di DeviceInput
		switch DeviceInputType(strings.ToLower(i.Type)) {
		case DeviceInputTypeButton:
			di = &DeviceInputButtonWithState{}
		case DeviceInputTypeController:
			di = &DeviceInputController{}
		default:
			return fmt.Errorf("unknown input type '%s' for '%s'", i.Type, s)
		}

		if err := i.unmarshal(di); err != nil {
			return err
		}

		inputMap[s] = di
	}

	*d = inputMap

	return nil
}

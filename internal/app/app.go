package app

import (
	"context"
	"time"

	"midilink/internal/config"
	"midilink/internal/device"
	"midilink/internal/events"
	"midilink/internal/obs"
	"midilink/internal/page"
)

type App struct {
	ctx           context.Context
	cancelFn      context.CancelFunc
	obsClient     *obs.ClientWrapper
	deviceManager *device.Manager
	pageManager   *page.Manager
	errorChan     chan error
}

func FromConfig(cfg *config.Config) (*App, error) {
	var err error
	deviceManager, err := device.NewManager(cfg.Midi, cfg.Debug)
	if err != nil {
		return nil, err
	}

	obsClient, err := obs.NewClientWrapper(cfg.Obs.Url, cfg.Obs.Password, cfg.Debug)
	if err != nil {
		return nil, err
	}

	pageManager, err := page.NewPageManager(obsClient, deviceManager)
	if err != nil {
		return nil, err
	}

	err = pageManager.Load(cfg.Pages)
	if err != nil {
		return nil, err
	}

	ctx, cancelFn := context.WithCancel(context.Background())

	return &App{
		ctx:           ctx,
		cancelFn:      cancelFn,
		obsClient:     obsClient,
		deviceManager: deviceManager,
		pageManager:   pageManager,
		errorChan:     make(chan error, 1),
	}, nil
}

func (c *App) Loop() {
	if c.ctx.Err() != nil {
		return
	}

	t := time.NewTicker(1)
	var connected = false
	for !connected {
		select {
		case <-c.ctx.Done():
			return
		case <-t.C:
			t.Reset(5 * time.Second)
			err := c.obsClient.Connect()
			if err == nil {
				connected = true
				t.Stop()
				break
			}

			c.sendError(err)
		}
	}

	err := c.pageManager.SetActivePage(page.KeyHome)
	if err != nil {
		c.sendError(err)
		return
	}

	c.sendError(nil)

	for {
		select {
		case e := <-c.deviceManager.Events():
			c.pageManager.DispatchEvent(e)
		case e := <-c.obsClient.IncomingEvents():
			if //goland:noinspection GoTypeAssertionOnErrors
			err, ok := e.(error); ok {
				c.sendError(err)
				c.Stop()
				return
			}

			c.pageManager.DispatchEvent(events.ObsEvent{Event: e})
		case <-c.ctx.Done():
			return
		}
	}
}

func (c *App) Stop() {
	if c.ctx.Err() != nil {
		return
	}

	c.cancelFn()
	c.pageManager.Close()
	c.deviceManager.Close()
	close(c.errorChan)
}

func (c *App) StopChan() <-chan struct{} {
	return c.ctx.Done()
}

func (c *App) sendError(err error) {
	select {
	case c.errorChan <- err:
	default:
		if len(c.errorChan) == cap(c.errorChan) {
			select {
			case <-c.errorChan:
			default:
			}

			c.errorChan <- err
		}
	}
}

func (c *App) Errors() <-chan error {
	return c.errorChan
}

func (c *App) ActivePageName() <-chan string {
	return c.pageManager.ActivePageName()
}

func (c *App) GetPageManager() *page.Manager {
	return c.pageManager
}

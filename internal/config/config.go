package config

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/goccy/go-yaml"
)

var (
	ErrUnsupportedConfigVersion = errors.New("unsupported config version")
)

type configVersionField struct {
	Version int `yaml:"version"`
}

type Config struct {
	Version int             `yaml:"version"`
	Debug   bool            `yaml:"debug"`
	Updater UpdaterConfig   `yaml:"updater"`
	Obs     OBS             `yaml:"obs"`
	Midi    Midi            `yaml:"midi"`
	Pages   map[string]Page `yaml:"pages"`
}

type UpdaterConfig struct {
	Disable    bool `yaml:"disable"`
	Prerelease bool `yaml:"prerelease"`
}

type OBS struct {
	Url      string `yaml:"url"`
	Password string `yaml:"password"`
}

type Midi struct {
	Driver  string            `yaml:"driver"`
	Devices map[string]Device `yaml:"devices"`
}

func Read(configFile string) (*Config, error) {
	fileName := filepath.Base(configFile)
	path, err := filepath.Abs(filepath.Dir(configFile))
	if err != nil {
		return nil, err
	}

	b, err := os.ReadFile(filepath.Join(path, fileName))
	if err != nil {
		return nil, err
	}

	var versionField configVersionField
	err = yaml.Unmarshal(b, &versionField)
	if err != nil {
		return nil, err
	}

	var config Config
	err = yaml.Unmarshal(b, &config)
	if err != nil {
		return nil, err
	}

	if config.Version != versionField.Version {
		return nil, ErrUnsupportedConfigVersion
	}

	for deviceName := range config.Midi.Devices {
		device := config.Midi.Devices[deviceName]
		if device.ProfileFile == "" {
			continue
		}

		b, err := os.ReadFile(filepath.Join(path, device.ProfileFile))
		if err != nil {
			return nil, fmt.Errorf("cannot read device profile for %s : %w", deviceName, err)
		}

		var profile DeviceProfile
		err = yaml.Unmarshal(b, &profile)
		if err != nil {
			return nil, fmt.Errorf("cannot read device profile for %s : %w", deviceName, err)
		}

		if len(device.Inputs) == 0 {
			device.Inputs = make(DeviceInputMap)
		}
		for s := range profile.Inputs {
			if _, ok := device.Inputs[s]; ok {
				continue
			}

			device.Inputs[s] = profile.Inputs[s]
		}

		config.Midi.Devices[deviceName] = device
	}

	return &config, nil
}

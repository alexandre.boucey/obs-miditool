package updater

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"sort"

	goversion "github.com/hashicorp/go-version"
	"github.com/mouuff/go-rocket-update/pkg/provider"
)

type GitlabDirect struct {
	Project     string
	ArchiveName string
	Prerelease  bool
}

var _ provider.Provider = (*GitlabDirect)(nil)

type gitlabRelease struct {
	Version *goversion.Version
	TagName string               `json:"tag_name"`
	Assets  *gitlabReleaseAssets `json:"assets"`
}

type gitlabReleaseAssets struct {
	Links []gitlabReleaseLink `json:"links"`
}

type gitlabReleaseLink struct {
	Name      string `json:"name"`
	DirectURL string `json:"direct_asset_url"`
}

func (c *GitlabDirect) getReleasesURL() (string, error) {
	return fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/releases",
		c.Project,
	), nil
}

func (c *GitlabDirect) getReleases() ([]gitlabRelease, error) {
	releasesURL, err := c.getReleasesURL()
	if err != nil {
		return nil, err
	}
	resp, err := http.Get(releasesURL) //nolint:noctx
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var releases []gitlabRelease
	err = json.NewDecoder(resp.Body).Decode(&releases)
	if err != nil {
		return nil, err
	}

	mapReleases := make(map[string]*gitlabRelease, len(releases))
	sortedVersions := make(goversion.Collection, 0, len(releases))
	for _, r := range releases {
		v, err := goversion.NewSemver(r.TagName)
		if err != nil {
			continue
		}

		if !c.Prerelease && v.Prerelease() != "" {
			continue
		}

		r.Version = v
		mapReleases[v.String()] = &r

		sortedVersions = append(sortedVersions, v)
	}

	sortedReleases := make([]gitlabRelease, len(sortedVersions))
	sort.Sort(sort.Reverse(sortedVersions))
	for i, v := range sortedVersions {
		sortedReleases[i] = *mapReleases[v.String()]
	}

	return sortedReleases, nil
}

// getReleases gets tags of the repository
func (c *GitlabDirect) getLatestRelease() (*gitlabRelease, error) {
	releases, err := c.getReleases()
	if err != nil {
		return nil, err
	}
	if len(releases) < 1 {
		return nil, errors.New("this gitlab project has no releases")
	}

	return &releases[0], nil
}

func (c *GitlabDirect) Open() error {
	return nil
}

func (c *GitlabDirect) Close() error {
	return nil
}

func (c *GitlabDirect) GetLatestVersion() (string, error) {
	release, err := c.getLatestRelease()
	if err != nil {
		return "", err
	}
	return release.Version.String(), nil
}

func (c *GitlabDirect) Walk(walkFn provider.WalkFunc) error {
	release, err := c.getLatestRelease()
	if err != nil {
		return err
	}

	for _, link := range release.Assets.Links {
		err = walkFn(&provider.FileInfo{
			Path: link.DirectURL,
			Mode: os.FileMode(0444),
		})

		if err != nil {
			return err
		}
	}

	return nil
}

func (c *GitlabDirect) Retrieve(src string, dest string) error {
	resp, err := http.Get(src) //nolint:noctx
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	tempFile, err := os.Create(dest)
	if err != nil {
		return err
	}
	_, err = io.Copy(tempFile, resp.Body)
	if err != nil {
		return err
	}

	err = tempFile.Sync()
	if err != nil {
		return err
	}

	return tempFile.Close()
}

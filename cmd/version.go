package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"midilink/internal/driver"
	"midilink/internal/updater"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of Midilink",
	Long:  `All software has versions. This is Midilink's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Midilink v%s - RtMidi v%s", updater.VersionString(), driver.GetVersion())
	},
}
